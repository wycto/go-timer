package crontab

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"os"
	"os/exec"
	"time"
)

type Crontab struct {
}

func (receiver *Crontab) Start(fileName string) {
	c := cron.New(cron.WithSeconds())
	c.AddFunc("*/3 * * * * ?", func() {
		fmt.Println("Every three Seconds")
		pwd, _ := os.Getwd()
		fmt.Println(pwd)

		fmt.Println("文件：", fileName)
		cmd := exec.Command("php", "-f", fileName)
		fmt.Println(cmd)

		err := cmd.Start()
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println(time.Now())
		fmt.Println()
	})
	c.Start()

	select {} //阻塞主线程停止
}
